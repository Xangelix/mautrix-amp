# mautrix-amp
A very hacky Matrix-SMS bridge based on running Android Messages for Web in Puppeteer.
This project is temporary and will eventually be replaced by a separate Android app.

Matrix room: [`#maunium:maunium.net`](https://matrix.to/#/#maunium:maunium.net)
