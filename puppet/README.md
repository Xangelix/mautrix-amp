### Listen config
If `type` is `unix`, `path` is the path where to create the socket.

If `type` is `tcp`, `port` and `host` are the host/port where to listen.

### Profile directory
The `profile_dir` specifies which directory to put chromium user data directories.
